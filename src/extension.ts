import * as vscode from 'vscode';

import * as util from 'util';
import * as fs from 'fs';
import * as path from 'path';


const stat = util.promisify(fs.stat);
const readdir = util.promisify(fs.readdir);


const COMPONENT_DIRECTORIES = [
	'applications',
	'framework',
	'plugins',
	'themes'
];


async function openAtLocation(filepath: string, anchorRe?: RegExp) {
	console.log('will try to open', filepath, 'with RegExp', anchorRe ? anchorRe.source : 'undefined');
	const editor = await vscode.window.showTextDocument(vscode.Uri.file(filepath));
	// go to beginnig of text
	editor.selections = [new vscode.Selection(0, 0, 0, 0)];
	if (anchorRe !== undefined) {
		const text = editor.document.getText();
		let lineNum = 0;
		for (const line of text.split('\n')) {
			if (anchorRe.exec(line)) {
				break;
			}
			lineNum++;
		}
		editor.selections = [new vscode.Selection(lineNum, 0, lineNum, 0)];
		editor.revealRange(new vscode.Range(lineNum, 0, lineNum, 0), vscode.TextEditorRevealType.InCenter);
	}

}

function isJavaDottedPath(javaDottedPath: string) {
	return /([^.]+\.[^.]+){2,}/.test(javaDottedPath);
}

async function findJavaComponentLocation(javaDottedPath: string) {
	const workspaceFolders = vscode.workspace.workspaceFolders;
	if (workspaceFolders === undefined) {
		vscode.window.showInformationMessage('only work in a workspace');
		return;
	}
	const javaPath = javaDottedPath.replace(/\./g, '/') + '.java';
	for (const folder of workspaceFolders) {
		for (const componentDir of COMPONENT_DIRECTORIES) {
			const components = await readdir(path.join(folder.uri.fsPath, componentDir));
			for (const component of components) {
				const javaFullPath = path.join(
					folder.uri.fsPath,
					componentDir,
					component,
					'src',
					'main',
					'java',
					javaPath
				);
				let statResult;
				try {
					statResult = await stat(javaFullPath);
				} catch {
					console.log(javaFullPath, 'seems to not exist');
					continue;
				}
				if (statResult.isFile()) {
					return javaFullPath;
				}
			}
		}
	}
}

async function findComponentLocation(xmlFile: string) {
	const match = /component:\/\/([^/]+)\//.exec(xmlFile);
	if (match === null) {
		if (isJavaDottedPath(xmlFile)) {
			return findJavaComponentLocation(xmlFile);
		}
		return;
	}
	const componentName = match[1];
	const workspaceFolders = vscode.workspace.workspaceFolders;
	if (workspaceFolders === undefined) {
		vscode.window.showInformationMessage('only work in a workspace');
		return;
	}
	for (const folder of workspaceFolders) {
		for (const componentDir of COMPONENT_DIRECTORIES) {
			const componentFullPath = path.join(folder.uri.fsPath, componentDir, componentName);
			console.log('will stat', componentFullPath, folder.uri.fsPath, componentDir, componentName);
			let statResult;
			try {
				statResult = await stat(componentFullPath);
			} catch {
				console.log(componentFullPath, 'seems to not exist');
				continue;
			}
			if (statResult.isDirectory()) {
				return path.join(componentFullPath, xmlFile.substring(match[0].length));
			}
		}
	}
}

async function matchLocationPageAttr(lineContent: string, currentDirectory: string) {
	let match = /(?:location|page|path)="([^"]+)"/.exec(lineContent);
	if (match !== null) {
		const location = match[1].split('#'),
			xmlFile = location[0],
			xmlAnchor = location[1];
		let xmlRe;
		if (xmlAnchor !== undefined) {
			xmlRe = new RegExp(`name="${xmlAnchor}"`);
		} else {
			const matchInvoke = /invoke="([^"]+)"/.exec(lineContent);
			if (matchInvoke !== null) {
				if (isJavaDottedPath(xmlFile)) {
					xmlRe = new RegExp(`\\s+public .* ${matchInvoke[1]}\\(\\w`);
				} else {
					xmlRe = new RegExp(`def ${matchInvoke[1]}\\(`);
				}
			}
		}
		console.log('location', location);
		if (!xmlFile.startsWith('component://') && !isJavaDottedPath(xmlFile)) {
			const fileLocation = path.join(currentDirectory, xmlFile);
			return openAtLocation(fileLocation, new RegExp(`name="${xmlAnchor}"`));
		}
		const fileLocation = await findComponentLocation(xmlFile);
		if (fileLocation === undefined) {
			vscode.window.showInformationMessage(`no file found for '${xmlFile}'`);
			return;
		}
		return openAtLocation(fileLocation, xmlRe);
	}
	return null;
}


async function main(activeTextEditor: vscode.TextEditor | undefined) {
	if (activeTextEditor === undefined) {
		return;
	}
	if (path.extname(activeTextEditor.document.fileName) !== '.xml') {
		vscode.window.showInformationMessage('not an xml file => nothing to do');
		return;
	}
    const currentDirectory = path.dirname(activeTextEditor.document.fileName);
	const position = activeTextEditor.selection.active;
	const currentLine = activeTextEditor.document.lineAt(position).text;
	if (await matchLocationPageAttr(currentLine, currentDirectory)) {
		return;
	} else {
		const match = /response.*type="view" value="([^"]+)"/.exec(currentLine);
		if (match === null) {
			return;
		}
		const viewName = match[1];
		const nbOfLine = activeTextEditor.document.lineCount;
		const endOfDocument = activeTextEditor.document.getText(
			new vscode.Range(nbOfLine - 50, 0, nbOfLine, 0)
		);
		const viewMapMatch = new RegExp(`view-map name="${viewName}".*`).exec(endOfDocument);
		if (viewMapMatch) {
			return matchLocationPageAttr(viewMapMatch[0], currentDirectory);
		}
	}
}



export function activate(context: vscode.ExtensionContext) {
	let disposable = vscode.commands.registerCommand('extension.ofbiz-jumpto', () => {

        main(vscode.window.activeTextEditor).catch(err => console.error('something wrong', err));
	});

	context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}
